import Head from "next/head";
import Image from "next/image";
import Pic from "../public/logo-dos-marketing.svg";
import Redes from "../public/redes.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWhatsapp, faInstagram } from "@fortawesome/free-brands-svg-icons";

export default function Home() {
  return (
    <>
      <Head>
        <title>LandingPage - dos Marketing</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <main
        className="flex flex-col  min-h-screen -z-20"
        style={{
          background: "url(background.png)",
          backgroundRepeat: "none",
          backgroundSize: "cover",
          objectFit:'fill'
        }}
      >
        <div className="px-12 py-6">
          <Image width={50} height={50} src={Pic} alt="Logo dos Marketing" />
        </div>
        <section className="container mx-auto flex flex-col px-8 sm:flex-col md:flex-row md:px-12 flex-1 justify-center content-center items-center w-full">
          <div className=":w-full xs:px-12 sm:w-full md:w-1/2 lg:w-1/2 ">
            <h1 className="text-5xl font-semibold mb-5">
              Creación de Contenido para{" "}
              <span className="text-cyan-300">REDES SOCIALES</span>
            </h1>
            <p className="text-xl font-light">
              Imágenes, videos y animaciones que necesitas para comunicar lo que
              quieres. También analítica digital.
            </p>

            <a
              rel="noreferrer"
              target="_blank"
              href="https://wa.me/+573145063381?text=Hola%2C%20quiero%20saber%20m%C3%A1s%20sobre%20el%20servicio%20de%20creaci%C3%B3n%20de%20contenido."
              className="flex items-center justify-center bg-indigo-500 md:w-full lg:w-1/2 p-2 rounded-3xl text-white font-semibold mt-8 hover:bg-indigo-600 hover:-translate-y-2"
            >
              Escribenos al Whtas’App{" "}
              <FontAwesomeIcon className="text-xl ml-5" icon={faWhatsapp} />
            </a>
          </div>

          <div className=" w-full mt-8 xs:w-full sm:w-full ">
            <Image
            className="flex-grow h-full w-full"
            src={Redes} alt="Logo dos Marketing" 
              style={{
                backgroundRepeat: "none",
                backgroundSize: "cover",
              }}
            />
          </div>
        </section>
        <div className="px-12 py-6">
          <a
            rel="noreferrer"
            target="_blank"
            className="w-10 h-10 bg-indigo-500 m-0 p-2 rounded-3xl text-white font-semibold mt-8 hover:bg-indigo-600 hover:-translate-y-2"
          >
            <FontAwesomeIcon className="text-xl" icon={faInstagram} />
          </a>
        </div>
      </main>
      <div
        style={{
          background: "url(Strip-Patterns.png)",
          backgroundRepeat: "none",
          backgroundSize: "cover",
        }}
        className="absolute top-0 left-0 h-full w-full -z-30"
      />
    </>
  );
}
